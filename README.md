# 深澜认证中心依赖包使用说明

> 语言

```angular2html
php
```

> 引入扩展包

```angular2html
1. 扩展包已放入packageList, 详情: https://packagist.org/packages/srun_zhanglf/phpfido2tool
2. 安装命令> composer require srun_zhanglf/phpfido2tool v1.0.0
3. 倘若由于镜像问题导致无法引入安装包，可在composer.json中设置国内镜像
...
"repositories": {
    "packagist": {
        "type": "composer",
        "url": "https://mirrors.aliyun.com/composer/"
    }
}
...
```

> 使用扩展包

```angular2html
* ################################################################
* ##### $host fido2认证中心地址，如: https://idp.srun.com:18081 #####
* ################################################################
* ##### $appid fido2认证中心分配应用id                          #####
* ################################################################
* ##### $appSecret fido2认证中心分配应用安全密钥                 #####
* ################################################################

依次调用如下函数
$obj = new Fido2Tool();
// 获取access_token授权
$obj->AppToken();
// 检测用户是否注册
$obj->RegisterUserExists();
// 开始注册
$obj->RegisterBegin();
// 完成注册
$obj->RegisterFinish();
// 开始认证
$obj->LoginBegin();
// 完成认证
$obj->LoginFinish();
```
