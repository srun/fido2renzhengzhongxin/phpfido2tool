<?php

namespace SrunZhanglf\Phpfido2tool;

/**
 * Fido2认证中心工具包
 *
 * $obj = new Fido2Tool();
 * ################################################################
 * ##### $host fido2认证中心地址，如: https://idp.srun.com:18081 #####
 * ################################################################
 * ##### $appid fido2认证中心分配应用id                          #####
 * ################################################################
 * ##### $appSecret fido2认证中心分配应用安全密钥                 #####
 * ################################################################
 *
 *
 * 获取授权
 * var_dump($obj->AppToken($host, $appid, $appSecret));
 * string(221) "{"code":200,"expire":"2022-12-12T11:01:39+08:00","token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBfaWQiOjE3Njc5NzUzOTEsImV4cCI6MTY3MDgxNDA5OSwib3JpZ19pYXQiOjE2NzA1NTQ4OTl9.fmXbl-gWiiwFgAeMxksW4rZoV7m6r0A23x9nyAwdm2U"}"
 *
 *
 * 检测是否已经注册
 * var_dump($obj->RegisterUserExists($host, $token, $username));
 * string(88) "{"code":4004,"message":"user not register","version":"1.0.0","error":"record not found"}"
 *
 *
 * 开始注册
 * var_dump($obj->RegisterBegin($host, $token, $username));
 * string(724) "{"code":200,"message":"OK","data":{"publicKey":{"challenge":"yeMxZ5BS1NWSKbVS7AN6yT8x0F+A0cl82lsyOaMfa9U=","rp":{"name":"webauthn","id":"idp.srun.com"},"user":{"name":"","id":"QAAAAAAAAAAAAA=="},"pubKeyCredParams":[{"type":"public-key","alg":-7},{"type":"public-key","alg":-35},{"type":"public-key","alg":-36},{"type":"public-key","alg":-257},{"type":"public-key","alg":-258},{"type":"public-key","alg":-259},{"type":"public-key","alg":-37},{"type":"public-key","alg":-38},{"type":"public-key","alg":-39},{"type":"public-key","alg":-8}],"authenticatorSelection":{"authenticatorAttachment":"null","requireResidentKey":false,"userVerification":"discouraged"},"timeout":60000,"attestation":"none"}},"total":1,"version":"1.0.0"}"
 *
 *
 * 完成注册
 * var_dump($obj->RegisterFinish($host,$token,$username,$bd1));
 * string(74) "{"code":400,"message":"redis: nil","version":"1.0.0","error":"redis: nil"}"
 *
 *
 * 开始认证
 * var_dump($obj->LoginBegin($host, $token, $username));
 * string(86) "{"code":400,"message":"record not found","version":"1.0.0","error":"record not found"}"
 *
 *
 *
 * 完成认证
 * var_dump($obj->LoginFinish($host, $token, $username, $bd2));
 * string(86) "{"code":400,"message":"record not found","version":"1.0.0","error":"record not found"}"
 */
class Fido2Tool
{
    const APP_TOKEN = '/app/token';            // FIDO2 获取access_token授权
    const REGISTER_USER_EXISTS_PATH = '/register/user-exists'; // FIDO2 用户是否注册
    const REGISTER_BEGIN = '/register/begin';       // FIDO2 开始注册
    const REGISTER_FINISH = '/register/finish';      // FIDO2 完成注册
    const LOGIN_BEGIN = '/login/begin';          // FIDO2 开始认证
    const LOGIN_FINISH = '/login/finish';         // FIDO2 完成认证

    /**
     * 获取access_token授权
     * @param $host      string 接口地址
     * @param $appId     string 应用编号
     * @param $appSecret string 应用密钥
     * @return mixed
     */
    public function AppToken($host, $appId, $appSecret)
    {
        $params = [
            'app_id' => $appId,
            'app_secret' => $appSecret
        ];
        $header = [
            'Content-Type: application/json'
        ];
        return Curl::postData($host . self::APP_TOKEN, json_encode($params), 'POST', false, $header);
    }

    /**
     * 用户是否注册
     * @param $host     string 接口地址
     * @param $token    string access_token授权
     * @param $username string 用户名
     * @return mixed
     */
    public function RegisterUserExists($host, $token, $username)
    {
        $params = [
            'username' => $username
        ];
        $header = [
            'Authorization: AccessToken ' . $token
        ];
        return Curl::postData($host . self::REGISTER_USER_EXISTS_PATH . '?' . http_build_query($params), '', 'GET', false, $header);
    }

    /**
     * 开始注册
     * @param $host     string 接口地址
     * @param $token    string access_token授权
     * @param $username string 用户名
     * @return mixed
     */
    public function RegisterBegin($host, $token, $username)
    {
        $params = [
            'username' => $username
        ];
        $header = [
            'Authorization: AccessToken ' . $token
        ];
        return Curl::postData($host . self::REGISTER_BEGIN . '?' . http_build_query($params), '', 'POST', false, $header);
    }

    /**
     * 完成注册
     * @param $host       string 接口地址
     * @param $token      string access_token授权
     * @param $username   string 用户名
     * @param $bodyParams string 前端传递上来的body请求体字符串
     * @return mixed
     */
    public function RegisterFinish($host, $token, $username, $bodyParams)
    {
        $params = [
            'username' => $username
        ];
        $header = [
            'Authorization: AccessToken ' . $token,
            'Content-Type: application/json;charset=utf-8'
        ];
        return Curl::postData($host . self::REGISTER_FINISH . '?' . http_build_query($params), $bodyParams, 'POST', false, $header);
    }

    /**
     * 开始认证
     * @param $host     string 接口地址
     * @param $token    string access_token授权
     * @param $username string 用户名
     * @return mixed
     */
    public function LoginBegin($host, $token, $username)
    {
        $params = [
            'username' => $username
        ];
        $header = [
            'Authorization: AccessToken ' . $token,
        ];
        return Curl::postData($host . self::LOGIN_BEGIN . '?' . http_build_query($params), '', 'GET', false, $header);
    }

    /**
     * 完成认证
     * @param $host       string 接口地址
     * @param $token      string access_token授权
     * @param $username   string 用户名
     * @param $bodyParams string 前端传递上来的body请求体字符串
     * @return mixed
     */
    public function LoginFinish($host, $token, $username, $bodyParams)
    {
        $params = [
            'username' => $username
        ];
        $header = [
            'Authorization: AccessToken ' . $token,
            'Content-Type: application/json;charset=utf-8'
        ];
        return Curl::postData($host . self::LOGIN_FINISH . '?' . http_build_query($params), $bodyParams, 'POST', false, $header);
    }
}