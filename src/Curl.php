<?php

namespace SrunZhanglf\Phpfido2tool;

class Curl
{
    /**
     * CURL操作
     *
     * @param string $url 请求的url
     * @param array|string|null $params 请求的参数  POST,PUT,DELETE 是数组  GET 是空
     * @param string $method 请求方法
     * @param bool $isBuild 是否要解析
     * @return mixed
     */
    public static function postData($url, $params, $method = 'POST', $isBuild = true, $header = [])
    {
        if (!$header) {
            $header = [
                'Content-Type: application/json;charset=utf-8'
            ];
        }

        $ch = curl_init(); //初始化curl
        curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);
        curl_setopt($ch, CURLOPT_URL, $url); //抓取指定网页
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header); //设置header
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //关闭 ssl 模块验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); //关闭 ssl 主机验证
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); //在发起连接前等待的时间，如果设置为0，则无限等待。
        curl_setopt($ch, CURLOPT_HEADER, 0); //启用时会将头文件的信息作为数据流输出

        $method = strtoupper($method);

        if ($isBuild && $params) {
            $params = http_build_query($params);
        }

        switch ($method) {
            case "GET" :
                curl_setopt($ch, CURLOPT_HTTPGET, true);
                break;
            case "POST":
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                break;
            case "PUT" :
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                break;
            case "DELETE":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                break;
        }

        $res = curl_exec($ch); //运行curl
        if ($res) {
            curl_close($ch);
        } else {
            $res = curl_error($ch); //将错误信息返回
            curl_close($ch);
        }
        return $res;
    }
}