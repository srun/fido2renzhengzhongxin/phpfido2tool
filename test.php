<?php
require_once __DIR__ . '/vendor/autoload.php';

use SrunZhanglf\Phpfido2tool\Fido2Tool;

$host = 'https://idp.srun.com:18081';
$appid = '1767975391';
$appSecret = 'b65555bd93a9635945581e30f1f87d5bc2ed2cea';
$token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBfaWQiOjE3Njc5NzUzOTEsImV4cCI6MTY3MDgxNDA2Nywib3JpZ19pYXQiOjE2NzA1NTQ4Njd9.J7_EVM7Pz_0xE1uTiqf2o0x8YZRKFI7-GThBdQhGQnc';
$username = 'zlf';
$bd1 = 'JSON.stringify({
    id: credential.id,
    rawId: bufferEncode(rawId),
    type: credential.type,
    response: {
        attestationObject: bufferEncode(attestationObj),
        clientDataJSON: bufferEncode(clientDataJson)
    }
})';
$bd2 = 'JSON.stringify({
    id: assertion.id,
    rawId: bufferEncode(assertion.rawId),
    type: assertion.type,
    response: {
        authenticatorData: bufferEncode(assertion.response.authenticatorData),
        clientDataJSON: bufferEncode(assertion.response.clientDataJSON),
        signature: bufferEncode(assertion.response.signature),
        userHandle: bufferEncode(assertion.response.userHandle)
    }
})';

$obj = new Fido2Tool();

/**
 * 获取授权
 * var_dump($obj->AppToken($host, $appid, $appSecret));
 * string(221) "{"code":200,"expire":"2022-12-12T11:01:39+08:00","token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBfaWQiOjE3Njc5NzUzOTEsImV4cCI6MTY3MDgxNDA5OSwib3JpZ19pYXQiOjE2NzA1NTQ4OTl9.fmXbl-gWiiwFgAeMxksW4rZoV7m6r0A23x9nyAwdm2U"}"
 */

/**
 * 检测是否已经注册
 * var_dump($obj->RegisterUserExists($host, $token, $username));
 * string(88) "{"code":4004,"message":"user not register","version":"1.0.0","error":"record not found"}"
 */
/**
 * 开始注册
 * print_r($obj->RegisterBegin($host, $token, $username));
 * string(724) "{"code":200,"message":"OK","data":{"publicKey":{"challenge":"yeMxZ5BS1NWSKbVS7AN6yT8x0F+A0cl82lsyOaMfa9U=","rp":{"name":"webauthn","id":"idp.srun.com"},"user":{"name":"","id":"QAAAAAAAAAAAAA=="},"pubKeyCredParams":[{"type":"public-key","alg":-7},{"type":"public-key","alg":-35},{"type":"public-key","alg":-36},{"type":"public-key","alg":-257},{"type":"public-key","alg":-258},{"type":"public-key","alg":-259},{"type":"public-key","alg":-37},{"type":"public-key","alg":-38},{"type":"public-key","alg":-39},{"type":"public-key","alg":-8}],"authenticatorSelection":{"authenticatorAttachment":"null","requireResidentKey":false,"userVerification":"discouraged"},"timeout":60000,"attestation":"none"}},"total":1,"version":"1.0.0"}"
 */
/**
 * 完成注册
 * var_dump($obj->RegisterFinish($host,$token,$username,$bd1));
 * string(74) "{"code":400,"message":"redis: nil","version":"1.0.0","error":"redis: nil"}"
 */
/**
 * 开始认证
 * var_dump($obj->LoginBegin($host, $token, $username));
 * string(86) "{"code":400,"message":"record not found","version":"1.0.0","error":"record not found"}"
 */
/**
 * 完成认证
 * var_dump($obj->LoginFinish($host, $token, $username, $bd2));
 * string(86) "{"code":400,"message":"record not found","version":"1.0.0","error":"record not found"}"
 */

